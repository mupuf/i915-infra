# CI latency

## Testing latency per project and test content

The CI system picks up patches from the different projects/mailing lists, compiles/deploys them, then run tests on it.
This view enables you to visualize the latency between a patch has been sent and when the results were published.

<div class="inline-form">
    Select a project / test to get statistics for:
    <select id="projects" onchange="pw_test_latency_fetch(this.options[this.selectedIndex].value, 'tests', 'plot', 1);">
        <option value="" disabled selected hidden>Please select a project</option>
    </select>
    <select id="tests" disabled>
        <option value="" selected hidden>Test</option>
    </select>
    <span></span>
</div>

<div id="plot"></div>

<script src='https://cdn.plot.ly/plotly-latest.min.js'></script>
<script type="text/javascript" src="/assets/latency.js"></script>
<script type="text/javascript">
    var projects = {
        "intel-gfx": {
            "name": "Intel GFX",
            "tests": {
                "Fi.CI.BAT": {"latency_target_hours": 0.5},
                "Fi.CI.CHECKPATCH": {"latency_target_hours": 0.5},
                "Fi.CI.DOCS": {"latency_target_hours": 0.5},
                "Fi.CI.SPARSE": {"latency_target_hours": 0.5},
                "Fi.CI.BUILD": {"latency_target_hours": 0.5},
                "Fi.CI.IGT": {"latency_target_hours": 6},
            }
        },
        "intel-gfx-trybot": {
            "name": "Intel GFX Trybot",
            "tests": {
                "Fi.CI.BAT": {"latency_target_hours": 6},
                "Fi.CI.BUILD": {"latency_target_hours": 6},
                "Fi.CI.IGT": {"latency_target_hours": 12},
            }
        },
        "igt": {
            "name": "IGT",
            "tests": {
                "Fi.CI.BAT": {"latency_target_hours": 0.5},
                "Fi.CI.BUILD": {"latency_target_hours": 0.5},
                "GitLab.Pipeline": {"latency_target_hours": 0.5},
                "Fi.CI.IGT": {"latency_target_hours": 6},
            }
        },
        "igt-trybot": {
            "name": "IGT trybot",
            "tests": {
                "Fi.CI.BAT": {"latency_target_hours": 6},
                "Fi.CI.BUILD": {"latency_target_hours": 6},
                "GitLab.Pipeline": {"latency_target_hours": 0.5},
                "Fi.CI.IGT": {"latency_target_hours": 12},
            }
        },
    };

    // Add all the available projects
    var e = document.getElementById('projects');
    for (let p_name in projects) {
        var o = document.createElement("option");
        o.value = p_name;
        o.text = projects[p_name].name;
        e.appendChild(o);
    }

    // If a project has been set in the URL, select it
    default_project = get_url_parameter('project', null);
    if (default_project !== null) {
        pw_test_latency_fetch(default_project, 'tests', 'plot', 1, get_url_parameter('test', undefined));
        document.getElementById('projects').value = default_project;
    }
</script>
