# Queues

## Queues

This page **does not** refresh automatically.

<div id="message"></div>
<table id="igt" class="queue">
   <colgroup>
     <col width="5%" />
      <col width="12%" />
      <col width="66%"/>
      <col width="17%"/>
    </colgroup>
    <tr><th>#</th><th>id</th><th>name</th><th>time posted</th></tr>
    <tr><th colspan="4">IGT BAT Queue <a href="#bat-queues">(?)</a></th></tr>
</table>

<table id="intel-gfx" class="queue">
   <colgroup>
     <col width="5%" />
      <col width="12%" />
      <col width="66%"/>
      <col width="17%"/>
    </colgroup>
    <tr><th colspan="4">Intel-GFX BAT Queue <a href="#bat-queues">(?)</a></th></tr>
</table>

<table id="igt-trybot" class="queue">
   <colgroup>
     <col width="5%" />
      <col width="12%" />
      <col width="66%"/>
      <col width="17%"/>
    </colgroup>
    <tr><th colspan="4">IGT Trybot BAT Queue <a href="#bat-queues">(?)</a></th></tr>
</table>


<table id="intel-gfx-trybot" class="queue">
   <colgroup>
     <col width="5%" />
      <col width="12%" />
      <col width="66%"/>
      <col width="17%"/>
    </colgroup>
    <tr><th colspan="4">Kernel Trybot BAT Queue <a href="#bat-queues">(?)</a></th></tr>
</table>


<script type="text/javascript" src="/assets/queues.js"></script>
<script type="text/javascript">
	query_timestamp("igt", "/queue/igt.timestamp");
	query_timestamp("intel-gfx", "/queue/intel-gfx.timestamp");
	query_timestamp("igt-trybot", "/queue/trybotigt.timestamp");
	query_timestamp("intel-gfx-trybot", "/queue/trybot.timestamp");
</script>

<br/>

<table id="shards" class="queue">
   <colgroup>
     <col width="5%" />
      <col width="12%" />
      <col width="66%"/>
      <col width="17%"/>
    </colgroup>
    <tr><th>#</th><th>id</th><th>name</th><th>time queued</th></tr>
    <tr><th colspan="4">Shards Queue <a href="#fullshards-queue">(?)</a></th></tr>
</table>

<script type="text/javascript">
	query_shards("shards", "/queue/pwqueue.txt");
</script>


## Explanations

### BAT Queues

The queues for **[Basic Acceptance Tests](/#basic-acceptance-tests-aka-bat)
runs**. Once the run is complete sharded run will be scheduled.

The queues are presented above in order of their priority. The first element
of a queue gets processed when all the preceding queues are empty.


### Full/Shards Queue

[Full/Sharded runs](/#full-igt-aka-sharded-runs) are queued after
corresponding [Basic Acceptance Test](/#basic-acceptance-tests-aka-bat) run
is completed, but they execute only if BAT is successful.

Due to technical limitation this is just **an approximation** of the queue.
It is good for assessing the length of the queue, but should not be
considered as completely accurate.

### CI Latency

If you are interested in knowning the testing latency's distribution, you
should check out the [dedicated latency page](/latency.html).
