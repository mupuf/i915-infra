var pw_latencies={};
var pw_project_cur = null;
var pw_plot_target = null;

function get_url_parameter(param, default_value) {
    const urlParams = new URLSearchParams(window.location.search);
    var value = urlParams.get(param);
    if (value === null)
        value = default_value
    return value
}

function set_url_parameter(param, value) {
    // Get the current list of parameters
    const urlParams = new URLSearchParams(window.location.search);
    var params = [];
    var found = false;
    urlParams.forEach(function(v, p) {
        if (p === param) {
            found = true;
            if (value === undefined)
                return;
            v = value;
        }
        params.push(`${p}=${v}`);
    });

    // Add the new one, if missing
    if (!found)
        params.push(`${param}=${value}`);

    // Reconstruct the URL
    l = window.location;
    url = `${window.location.href.split('?')[0]}?${params.join('&')}`;

    // Set the URL
    history.replaceState({}, "", url);
}

function pw_test_latency_fetch(project, tests_target, plot_target, weeks, default_test) {
    function setProgress(data, cur_page) {
        let text = "LOADING";
        if (data !== null) {
            let page_size = data.results.length;
            let page_count = Math.ceil(data.count / page_size);
            text = `LOADING ${cur_page} / ${page_count}`;
        }

        var loading_option = document.getElementById('test_loading_option');
        if (loading_option === null) {
            var loading_option = document.createElement("option");
            loading_option.selected = true;

            var select_tests = document.getElementById(tests_target);
            select_tests.appendChild(loading_option);
        }
        loading_option.text = text;
    }

    function getLatencyTrendData(cur_page, url = apiUrl, json_data = []) {
        if (cur_page === 0)
            setProgress(null, cur_page);

        return new Promise((resolve, reject) => fetch(url)
            .then(response => {
                if (response.status !== 200)  {
                    throw `${response.status}: ${response.statusText}`;
                }
                response.json().then(data => {
                    json_data = json_data.concat(data.results);

                    cur_page += 1;
                    setProgress(data, cur_page);

                    if(data.next) {
                        getLatencyTrendData(cur_page, data.next, json_data).then(resolve).catch(reject)
                    } else {
                        resolve(json_data);
                    }
                }).catch(reject);
            }).catch(reject));
    }

    function FormatData(json_data){
        json_data.forEach(item =>{
            var sdate = new Date(item.event_time);
            var elapsedTime =  (sdate - new Date(item.parameters.revision_completed)) / (1000*60*60);

            var format = Plotly.d3.time.format("WW%Y'%U.%w");
            var label = format(sdate);

            if (!pw_latencies[item.parameters.test]) pw_latencies[item.parameters.test] = {};
            if (!pw_latencies[item.parameters.test][label]) pw_latencies[item.parameters.test][label] = [];
            pw_latencies[item.parameters.test][label].push(elapsedTime)
        });

        var select_tests = document.getElementById(tests_target);

        // Remove all the current options
        var child = select_tests.lastElementChild;
        while (child) {
            select_tests.removeChild(child);
            child = select_tests.lastElementChild;
        }

        // Add all the new options
        Object.keys(pw_latencies).forEach(test_name=>{
            var option = document.createElement("option");
            option.text = test_name;
            option.value = test_name.trim();
            option.addEventListener('click', function(e) {pw_latency_plot(e.target.value);});
            select_tests.appendChild(option);
        });
        select_tests.disabled = false;
    }

    // Reset everything
    pw_latencies = {};
    pw_project_cur = project;
    pw_plot_target = plot_target;
    set_url_parameter('project', pw_project_cur);

    let date = new Date();
    date.setHours(0, 0, 0, 0)
    let since = new Date(date.getTime() - (7 * 24 * 60 * 60 * 1000));
    const apiUrl=`https://patchwork.freedesktop.org/api/1.0/projects/${project}/events/?format=json&name=new-test-result&since=${since.toISOString()}`;
    getLatencyTrendData(0)
        .then(json_data => {
            FormatData(json_data);
            test = default_test !== undefined ? default_test : Object.keys(pw_latencies)[0];
            pw_latency_plot(test);
            document.getElementById(tests_target).value=test;
        })
        .catch(console.error);
}

function pw_latency_plot(test_name){
    let project = projects[pw_project_cur];
    let test = project.tests[test_name] || {"latency_target_hours": null};
    let perWeekData = pw_latencies[test_name];

    set_url_parameter('test', test_name);

    let x_labels = [];
    let y_latencies = [];
    for (var j = 0; j < Object.keys(perWeekData).length; j++) {
        let label = Object.keys(perWeekData)[j];
        let samples = Object.values(perWeekData)[j];
        let x_label = `${label} (${samples.length})`

        samples.forEach(item => {
            y_latencies.unshift(item);
            x_labels.unshift(x_label);
        });
    }

    let graphs = [];
    if (test.latency_target_hours !== null) {
        let object_line_data = {
            x: [""].concat(x_labels.concat([" "])),
            y: Array(x_labels.length + 2).fill(test.latency_target_hours),
            mode: 'lines',
            name: 'Median target',
            line: {color: 'red'},
            type: 'scatter'
        };
        graphs.push(object_line_data);
    }

    var violin_data = {
        type: 'violin',
        hoveron: "violins+points",
        x: x_labels,
        y: y_latencies,
        name: 'Distribution',
        line: {
            color: 'skyblue',
            width: 2
        },
        box: {
            visible: true
        },
        boxpoints: true,
        fillcolour: 'rgba(162, 177, 198, 0.5)',
        medianline: {
            color: 'black',
            visible: true
        }
    }
    graphs.push(violin_data);

    var layout = {
        title: `Testing latency of <a href="https://patchwork.freedesktop.org/project/${pw_project_cur}/series">${projects[pw_project_cur].name}</a>'s ${test_name}`,
        "yaxis": {
            "type": "linear",
            "title": "Latency (hours)",
            "rangemode": "nonnegative",
            "autorange": true,
            "zeroline": false,
            "zerolinewidth": 1
        },
        "xaxis": {
            "title": "Day (# series tested)",
        },
        violingap: 0,
        violingroupgap: 0,
        violinmode: "group",
    }
    Plotly.newPlot(pw_plot_target, graphs, layout);
}
